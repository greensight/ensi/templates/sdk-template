<?php

namespace Ensi\BackendServiceClient\Api;

use Ensi\BackendServiceClient\Dto\Example\ExampleRequest;
use Ensi\BackendServiceClient\Dto\Example\ExampleResponse;
use Ensi\BackendServiceClient\RequestBuilder;
use GuzzleHttp\Promise\PromiseInterface;

class ExampleApi extends BaseApi
{
    public function example(ExampleRequest $request): ExampleResponse
    {
        return $this->send($this->exampleRequest($request), fn ($content) => new ExampleResponse($content));
    }

    public function exampleAsync(ExampleRequest $request): PromiseInterface
    {
        return $this->sendAsync($this->exampleRequest($request), fn ($content) => new ExampleResponse($content));
    }

    protected function exampleRequest(ExampleRequest $requestDto): RequestBuilder
    {
        return (new RequestBuilder('/example/endpoint', 'GET'))->json($requestDto);
    }
}
