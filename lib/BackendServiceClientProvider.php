<?php

namespace Ensi\BackendServiceClient;

class BackendServiceClientProvider
{
    public static array $apis = [
        '\Ensi\BackendServiceClient\Api\ExampleApi',
    ];

    public static string $configuration = Configuration::class;
}
