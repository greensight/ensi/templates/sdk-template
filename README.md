# BackendServiceClient

# Подключение к сервису

Детали разворота и подключения sdk описана [тут](https://docs.ensi.tech/backend-guides/principles/http-client#шаблон-для-sdk-пакетов) 

## Requirements

PHP 8.0 and later

## Installation & Usage

### Composer

To install the bindings via [Composer](http://getcomposer.org/), add the following to `composer.json`:

```json
{
  "repositories": [
    {
      "type": "vcs",
      "url": "https://gitlab.com/greensight/ensi/templates/sdk-template.git"
    }
  ],
  "require": {
    "ensi/backend_service-client": "dev-master"
  }
}
```

Then run `composer install`

## Author

mail@greensight.ru

